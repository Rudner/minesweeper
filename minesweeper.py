import sys
import itertools
import random
import time
from copy import deepcopy

import pygame as pg

# ---------------------- OPTIONS -----------------------------------------------

DIFFICULTY = 'expert'
AUTOCLICK_OBVIOUS_CELLS = True

# ---------------------- CONSTANTS ---------------------------------------------

BOARD_WIDTH, BOARD_HEIGHT, MINES = {
    'beginner': (10, 10, 10),
    'intermediate': (16, 16, 40),
    'expert': (30, 16, 99),
}[DIFFICULTY]

TILE_SIZE = 40
WIDTH = TILE_SIZE * BOARD_WIDTH
HEIGHT = TILE_SIZE * BOARD_HEIGHT
FONT_SIZE = 35

BLUE  = (0, 0, 255)
RED   = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
GREY = (100, 100, 100)
GREY2 = (128, 128, 128)
WHITE = (255, 255, 255)
PURPLE = (255, 0, 255)
MAROON = (128, 0, 0)
TURQUOISE = (64,224,208)
PINK = (255, 128, 128)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

NUM_MINES_COLOUR = [
    GREY,
    BLUE,
    GREEN,
    PURPLE,
    MAROON,
    PINK,
    YELLOW,
    TURQUOISE,
]

LEFT_CLICK = 1
RIGHT_CLICK = 3

# ----------------------- INIT -------------------------------------------------

pg.init()
screen = pg.display.set_mode((WIDTH + 1 + 100, HEIGHT + 1))

font = pg.font.SysFont("monospace", FONT_SIZE, bold=True)
game_over_font = pg.font.SysFont("monospace", 100, bold=True)

# ---------------------- FUNCTIONS ---------------------------------------------

def draw_text(text, pos, color=GREY, centered=True, font=font):
    label = font.render(str(text), 1, color)
    if centered: pos = label.get_rect(center=pos)
    screen.blit(label, pos)

def get_mouse_pos():
    x, y = pg.mouse.get_pos()
    return x // TILE_SIZE, y // TILE_SIZE

def count(values, property, value=True):
    return sum(getattr(val, property) == value for val in values)

def solvable(board):
    board = deepcopy(board)


# ---------------------- CLASSES -----------------------------------------------

class Tile:
    mine = False
    visible = False
    flagged = False

    def __init__(self, board, x, y):
        self.board = board
        self.x = x
        self.y = y

        left = self.x * TILE_SIZE + 1
        top = self.y * TILE_SIZE + 1
        self.rect = pg.Rect(left, top, TILE_SIZE - 1, TILE_SIZE - 1)

    def draw(self):
        if self.flagged:
            pg.draw.rect(screen, RED, self.rect)
        elif not self.visible:
            pg.draw.rect(screen, WHITE, self.rect)
        elif self.mine:
            # pg.draw.rect(screen, BLACK, self.rect)
            pg.draw.rect(screen, GREY, self.rect)
            pg.draw.circle(screen, BLACK, self.rect.center, TILE_SIZE // 3)
        else:
            # pg.draw.rect(screen, WHITE if self.mines else GREY, self.rect)
            pg.draw.rect(screen, GREY, self.rect)
            draw_text(self.mines, self.rect.center, color=NUM_MINES_COLOUR[self.mines])

    @property
    def num(self):
        return None if not self.visible else count(self.neighbours, 'mine')

    def onClick(self):
        if not self.board.assigned:
            self.board.try_assign(self.x, self.y)
        if self.visible:
            if count(self.neighbours, 'flagged') >= self.num:
                for x in self.neighbours:
                    if not x.visible and not x.flagged:
                        x.onClick()
        if self.visible or self.flagged:
            return
        self.board.uncovered += 1
        self.visible = True
        self.mines = sum(1 if x.mine else 0 for x in self.neighbours)
        self.draw()
        if self.mine:
            board.game_over = True
            draw_text('GAME OVER', (WIDTH // 2, HEIGHT // 2), font=game_over_font, color=YELLOW)
            return
        if board.all_uncovered:
            board.game_over = True
            draw_text('VICTORY', (WIDTH // 2, HEIGHT // 2), font=game_over_font, color=YELLOW)
            return
        if self.mines == 0:
            for x in self.neighbours:
                x.onClick()

    def onRightClick(self):
        if self.visible: return
        if self.flagged:
            board.flagged -= 1
            self.flagged = False
        else:
            board.flagged += 1
            self.flagged = True
        self.draw()

    @property    
    def neighbours(self):
        yield from filter(None, (
            self.board[self.x + s, self.y + t]
            for s, t in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
        ))


class Board:

    uncovered = 0
    flagged = 0
    assigned = False
    game_over = False
    start = None

    def __init__(self, width, height, mines):
        self.width = width
        self.height = height
        self.mines = mines
        self.tiles = [[Tile(self, x, y) for x in range(width)] for y in range(height)]
        self.draw()

    def try_assign(self, x, y):
        self.assigned = True
        while True:
            mines = random.sample(list(self), MINES)
            for tile in mines:
                tile.mine = True
            if self[x, y].mine or any(tile.mine for tile in self[x, y].neighbours):
                for tile in mines:
                    tile.mine = False
                continue
            self.start = time.time()
            return

    def __len__(self):
        return self.width * self.height

    def __iter__(self):
        yield from itertools.chain(*self.tiles)

    def __getitem__(self, k):
        x, y = k
        if not (0 <= x < BOARD_WIDTH and 0 <= y < BOARD_HEIGHT):
            return None
        return self.tiles[y][x]

    @property
    def all_uncovered(self):
        return self.uncovered == len(self) - self.mines

    def draw(self):
        for tile in self:
            tile.draw()

class Counter:

    def __init__(self, left, top, width, height):
        self.rect = pg.Rect(left, top, width, height)
        self.value = 0
        self.draw()

    def update(self, value):
        self.value = value
        self.draw()

    def draw(self):
        pg.draw.rect(screen, BLACK, self.rect)
        draw_text(f'{self.value:.0f}', self.rect.center, color=YELLOW)

# ---------------------- LOGIC -------------------------------------------------

board = Board(BOARD_WIDTH, BOARD_HEIGHT, MINES)
timer = Counter(WIDTH, 0, 100, 100)
mines = Counter(WIDTH, 100, 100, 100)

def quit():
    pg.quit()
    sys.exit()

while True:
    pg.display.update()

    if not board.game_over and board.start is not None:
        elapsed = time.time() - board.start
        timer.update(elapsed)

    mines.update(board.mines - board.flagged)

    if AUTOCLICK_OBVIOUS_CELLS:
        for tile in board:
            if board.game_over:
                break
            if count(tile.neighbours, 'flagged') == tile.num:
                tile.onClick()
            if count(tile.neighbours, 'visible', False) == tile.num:
                for neighbour in tile.neighbours:
                    if not neighbour.visible and not neighbour.flagged:
                        neighbour.onRightClick()

    for event in pg.event.get():
        if event.type == pg.QUIT: quit()
        if board.game_over: continue
        if event.type == pg.MOUSEBUTTONDOWN:
            if not board[get_mouse_pos()]:
                continue
            if event.button == LEFT_CLICK:
                board[get_mouse_pos()].onClick()
            if event.button == RIGHT_CLICK:
                board[get_mouse_pos()].onRightClick()
