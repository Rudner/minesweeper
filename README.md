# Minesweeper

My implementation of Minesweeper using Pygame.

A timer and the number of remaining mines are shown on the right.

Controls are:
1. Left-click on a hidden tile to reveal it.
2. Right-click on a hidden tile to mark it as a mine.
3. Left-click on a revealed tile who has the correct number of surrounding tiles marked (correctly or incorrectly) to reveal unmarked surrounding tiles.

Set `AUTOCLICK_OBVIOUS_MINES` to True to:
1. For each revealed tile, if it shows _x_ and has only _x_ hidden neighbours,
mark each hidden neighbour as a mine.
2. For each revealed tile, if it shows _x_ and has _x_ marked neighbours, reveal
each unmarked neighbour.
